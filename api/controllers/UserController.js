/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `UserController.create()`
   */
  create: function(req, res) {

    User.create({
      name: req.param('name'),
      email: req.param('email'),
      password: req.param('c-password'),
    }).exec(function(error, item) {
      if (error) {
        console.log(error);
      } else {
        console.log(item);
      }
      User.find().exec(function(error, users) {
        if (error) return error;
        res.redirect('/');
      });

    });
  },


  /**
   * `UserController.update()`
   */
  update: function(req, res) {
    return res.json({
      todo: 'update() is not implemented yet!'
    });
  },


  /**
   * `UserController.delete()`
   */
  delete: function(req, res) {
    return res.json({
      todo: 'delete() is not implemented yet!'
    });
  },

  index: function(req, res) {
    User.find().exec(function(error, users) {
      if (error) return error;

      res.view({
        title: 'home',
        users: users
      });
    });


  }


};